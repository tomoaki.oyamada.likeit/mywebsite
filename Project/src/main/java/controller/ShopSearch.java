package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.ShopDao;

/**
 * Servlet implementation class ShopSearch
 */
@WebServlet("/ShopSearch")
public class ShopSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");
	  
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_search.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
	   String name=request.getParameter("name");
	   String lowPrice=request.getParameter("low-price");
	   String maxPrice=request.getParameter("max-price");
	   String place=request.getParameter("place");
	/*   String service=request.getParameter("service");
	   String atomsphere=request.getParameter("atomsphere");
       String clean=request.getParameter("clean");
	   String volume=request.getParameter("volume");
	   String taste=request.getParameter("taste");*/
	   request.setAttribute("inputName", name);
       request.setAttribute("inputLowPrice", lowPrice);
       request.setAttribute("inputMaxPrice", maxPrice);
       request.setAttribute("inputPlace",place);
       
	   
	   
	  //店名　金額　場所だけにする 
	   ShopDao shop =new ShopDao();
	List<ShopPostDataBeans> shopList=shop.search(name,lowPrice,maxPrice,place);
	   request.setAttribute("shopList", shopList);
	  
	   RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_search.jsp");
	      dispatcher.forward(request, response);
	}

}
