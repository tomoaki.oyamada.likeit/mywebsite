package controller;


import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;
import beans.ShopBeans;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.AverageDao;
import dao.ShopDao;
import dao.ShopPostDao;


/**
 * Servlet implementation class ShopTreadAdd
 */
@WebServlet("/ShopPostAdd")
@MultipartConfig 
public class ShopPostAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopPostAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
	  request.setCharacterEncoding("UTF-8");
	  
      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      if (user == null) {
        
        request.setAttribute("errMsg", "ログインしてください。");
        response.sendRedirect("Login");
        return;
      }
      
      
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("UTF-8");
	     
	 String name =request.getParameter("name");
	  ShopDao shop = new ShopDao();
	  ShopBeans targetshop = shop.findByShop(name);
	  String shopName = targetshop.getShopName();
	 
	 
	 String Score = request.getParameter("score");
	 String Price=request.getParameter("price");
     String Service=request.getParameter("service");
	 String Volume =request.getParameter("volume");
	 String Atomsphere=request.getParameter("atomsphere");
	 String Clean =request.getParameter("clean");
	 String Taste=request.getParameter("taste");


//  BufferedImage bufferedImage = ImageIO.read(new File(request.getParameter("file")));
	Part part = request.getPart("file");
	 InputStream inputStream = (InputStream) part.getInputStream();

     byte[] img = convertInputStreamToByteArray(inputStream);

    

	      // ファイルのコンテンツタイプをしらべる
  //   String contentType = img.probeContentType();
//      String path = img.getPath();
//	 byte[] bytesData = {};
//     FileInputStream is = (FileInputStream) part.getInputStream();
     // ファイル内容をbyte[]に読み込む
//	byte[] bytes = is
//ファイルが読み込めてない？　ここがおかしいい
  
//     String file = Base64.getEncoder().encodeToString(img);
  
     // data URIを作る
 /*    StringBuilder sb = new StringBuilder();
     sb.append("data:");
     sb.append(contentType);
     sb.append(";base64,");
     sb.append(base64);
     
     String file = sb.toString();*/
//	 String path = file_.getAbsolutePath();
//     byte[] File= path.getBytes();
//     String file= Base64.getEncoder().encodeToString(File);
     String body=request.getParameter("body");
     
     if(!(name.equals(shopName))){
       request.setAttribute("errMsg", "登録した店舗名を入力してください");

       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
       dispatcher.forward(request, response);
       return;
     }
     
     if (name.equals("")|| Score.equals("")|| Price.equals("") ||Atomsphere.equals("") ||Clean.equals("") ||
        Volume.equals("") || Taste.equals("")||Service.equals("")){
       request.setAttribute("errMsg", "入力された内容は正しくありません");
       request.setAttribute("inputName", name);
       request.setAttribute("inputScore", Score);
       request.setAttribute("inputPrice", Price);
       request.setAttribute("inputService",Service);
       request.setAttribute("inputVolume", Volume);
       request.setAttribute("inputAtomsphere",Atomsphere);
       request.setAttribute("inputClean", Clean);
//       request.setAttribute("inputFile", file);
       request.setAttribute("inputBody", body);
       
       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
       dispatcher.forward(request, response);
       return;
     }

   
     int atomsphere=Integer.parseInt(Atomsphere);
     int clean = Integer.parseInt(Clean);
     int score = Integer.parseInt(Score);
     int price = Integer.parseInt(Price); 
     int service=Integer.parseInt(Service);
     int taste = Integer.parseInt(Taste); 
     int volume = Integer.parseInt(Volume);
     
     
     
     if (score<0||score>100){
       request.setAttribute("errMsg", "0～100の点数を入れてください");
       request.setAttribute("inputName", name);
       request.setAttribute("inputScore", Score);
       request.setAttribute("inputPrice", Price);
       request.setAttribute("inputService",Service);
       request.setAttribute("inputVolume", Volume);
       request.setAttribute("inputAtomsphere",Atomsphere);
       request.setAttribute("inputClean", Clean);
//       request.setAttribute("inputFile", file);
       request.setAttribute("inputBody", body);

       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
       dispatcher.forward(request, response);
       return;
     }
     HttpSession session = request.getSession();
     UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

     request.setAttribute("postMsg", "投稿が完了しました。");
     
     ShopPostDao post =new ShopPostDao();
     int shop_id=targetshop.getShopId();
     int id= user.getId();
     
     post.insertPost(name, score, price, clean, service, volume, taste,atomsphere, body,id,shop_id);
//     shop.insertShopfile(file,shop_id);

     shop.insertShopImg(img,shop_id);
       
    

     RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
     dispatcher.forward(request, response);
     }

  private byte[] convertInputStreamToByteArray(InputStream inputStream) throws IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    int nRead;
    byte[] data = new byte[16777215];
    while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
        buffer.write(data, 0, nRead);
    }
    return buffer.toByteArray();

  }


  }


