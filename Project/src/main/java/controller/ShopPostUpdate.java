package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.ShopDao;
import dao.ShopPostDao;

/**
 * Servlet implementation class ShopTreadUpdate
 */
@WebServlet("/ShopPostUpdate")
public class ShopPostUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopPostUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      if (user == null) {
        
        request.setAttribute("errMsg", "ログインしてください。");
        response.sendRedirect("Login");
        return;
      }
      int postId= Integer.parseInt(request.getParameter("postId"));
      ShopPostDao shopPostDao = new ShopPostDao();
      ShopPostDataBeans targetPost =shopPostDao.findByPostld(postId);
      request.setAttribute("postId",targetPost);
      
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_update.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  String name =request.getParameter("name");
	     String Score = request.getParameter("score");
	     String Price=request.getParameter("price");
	     String Place = request.getParameter("place");
	     String Service=request.getParameter("service");
	     String Volume =request.getParameter("volume");
	     String Atomsphere=request.getParameter("atomsphere");
	     String Clean =request.getParameter("clean");
	     String Taste=request.getParameter("taste");
	     String file=request.getParameter("file");
	     String body=request.getParameter("body");
	     int atomsphere=Integer.parseInt(Atomsphere);
	     int clean = Integer.parseInt(Clean);
	     int score = Integer.parseInt(Score);
	     int price = Integer.parseInt(Price); 
	     int place= Integer.parseInt(Place);
	     int service=Integer.parseInt(Service);
	     int taste = Integer.parseInt(Taste); 
	     int volume = Integer.parseInt(Volume);
	     
	     if (name.equals("") || Score.equals("")|| Price.equals("") ||Atomsphere.equals("") ||Clean.equals("") ||
	         Place.equals("")||Volume.equals("") || Taste.equals("")||Service.equals("")){
	       request.setAttribute("errMsg", "入力された内容は正しくありません");
	       request.setAttribute("inputName", name);
	       request.setAttribute("inputScore", Score);
	       request.setAttribute("inputPrice", Price);
	       request.setAttribute("inputPlace", place);
	       request.setAttribute("inputService", service);
	       request.setAttribute("inputVolume", volume);
	       request.setAttribute("inputAtomsphere",atomsphere);
	       request.setAttribute("inputClean", clean);
	       request.setAttribute("inputFile", file);
	       request.setAttribute("inputBody", body);


	       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_update.jsp");
	       dispatcher.forward(request, response);
	       return;
	     }
	     
	     request.setAttribute("postMsg", "投稿が完了しました");
	     String PostId= request.getParameter("postId"); 
	     int postId=Integer.parseInt(PostId);
	     ShopPostDao post =new ShopPostDao();
	     post.updatePost(name, score, price, clean, service, volume, taste,atomsphere,body,postId);
	     response.sendRedirect("UserPostList");
	     
	}

}
