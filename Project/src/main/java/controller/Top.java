package controller;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopBeans;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.AverageDao;
import dao.ImageDao;
import dao.ShopDao;
import dao.ShopPostDao;

/**
 * Servlet implementation class Top
 */
@WebServlet("/Top")
public class Top extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Top() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      ShopDao shop = new ShopDao();
      AverageDao average = new AverageDao();
      ShopDataBeans rank1 = average.findShopRank1();
     
      int shopId = rank1.getShopId();
      ShopBeans shop1 = shop.findByShop2(shopId);
      request.setAttribute("shopId", shopId);
      request.setAttribute("shop1", shop1);
      request.setAttribute("rank1", rank1);
      
  /*    ImageDao image = new ImageDao();
      byte[] img =ImageDao.getImageById(shopId);
      request.setAttribute("img", img);*/
   /*  shop1.getImg();
       File 
       String path = ;

    // Base64をPNGまたはJPG画像に変換する
       OutputStream fos = new OutputStream(path);
 */
       
       ShopDataBeans rank2 = average.findShopRank2();
       int shopId2 = rank2.getShopId();
       ShopBeans shop2 = shop.findByShop2(shopId2);
       request.setAttribute("shopId2", shopId2);
       request.setAttribute("shop2", shop2);
       request.setAttribute("rank2", rank2);
       
       ShopDataBeans rank3 = average.findShopRank3();
       int shopId3 = rank3.getShopId();
       ShopBeans shop3 = shop.findByShop2(shopId3);
       request.setAttribute("shopId3", shopId3);
       request.setAttribute("shop3", shop3);
       request.setAttribute("rank3", rank3);


       
  /*int shopId = Integer.parseInt(request.getParameter("postId"));
      request.setAttribute("id", shopId);
      ShopPostDao shop= new ShopPostDao();
      List<ShopPostDataBeans> shopPostList= shop.findShopAll(shopId);
      ShopPostDataBeans place=shopPostList.get(5);
      ShopPostDataBeans shopName=shopPostList.get(0);
 o     
      AverageDao average = new AverageDao();
      double scoreAverage= average.getScoreAverage(shopId);
      double serviceAverage= average.getServiceAverage(shopId);
      double volumeAverage= average.getVolumeAverage(shopId);
      double priceAverage= average.getPriceAverage(shopId);
      int tasteAverage = average.getTasteAverage(shopId);
   
 ShopDao shopDao = new ShopDao();
 ShopDao ShopBeans = shopDao.insertPostAverage(shopId, shopName, scoreAverage, serviceAverage, volumeAverage, priceAverage, tasteAverage, place );
  
	  */
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
