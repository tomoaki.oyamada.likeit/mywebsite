package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.ShopPostDao;

/**
 * Servlet implementation class ShopTreadList
 */
@WebServlet("/ShopPostList")
public class ShopPostList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopPostList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      
      if (user == null) {
        
        request.setAttribute("errMsg", "ログインしてください。");
        response.sendRedirect("Login");
        return;
      }
      int postId = Integer.parseInt(request.getParameter("postId"));
      int shopId = Integer.parseInt(request.getParameter("shopId"));
      
      ShopPostDao post= new ShopPostDao();
      int id=user.getId();
      List<ShopPostDataBeans> shopPostList = post.findShopAll(shopId);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("shopPostList", shopPostList);
       
      
     RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_list.jsp");
     dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
