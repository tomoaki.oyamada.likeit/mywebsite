package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserDataBeans;
import dao.UserDao;
import passwordEnco.PasswordEncorder;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     request.setCharacterEncoding("UTF-8");
	     String loginId = request.getParameter("loginId");
	     String password = request.getParameter("password");
	     
	        UserDao userDao = new UserDao();
	        PasswordEncorder pe = new PasswordEncorder();
          String Password= pe.encordPassword(password);
	       
	        UserDataBeans user = userDao.findByLoginInfo(loginId,Password);
	     /** テーブルに該当のデータが見つからなかった場合 * */
	        if (user == null) {
	          // リクエストスコープにエラーメッセージをセット
	          request.setAttribute("errMsg", "ログインに失敗しました。");
	          // 入力したログインIDを画面に表示するため、リクエストに値をセット
	          request.setAttribute("loginId", loginId);

	          // ログインjspにフォワード
	          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
	          dispatcher.forward(request, response);
	          return;
	        }
	        // セッションにユーザの情報をセット
	        HttpSession session = request.getSession();
	        session.setAttribute("userInfo", user);
	         
	          response.sendRedirect("Top");
	        

	 
	}

}
