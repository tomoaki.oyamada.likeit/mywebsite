package controller;



import java.io.IOException;
import java.util.List;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopBeans;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.AverageDao;
import dao.ShopDao;
import dao.ShopPostDao;


/**
 * Servlet implementation class ShopTreadAdd
 */
@WebServlet("/ShopAdd")
public class ShopAdd extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      if (user == null) {
        
        request.setAttribute("errMsg", "ログインしてください。");
        response.sendRedirect("Login");
        return;
      }
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_add.jsp");
      dispatcher.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");
         
     String name =request.getParameter("name");
     String Place = request.getParameter("place");
     
 
     
     if (name.equals("") ||Place.equals("")){
       request.setAttribute("errMsg", "入力された内容は正しくありません");
       request.setAttribute("inputName", name);
       request.setAttribute("inputPlace", Place);

       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
       dispatcher.forward(request, response);
       return;
     }

     ShopDao shop = new ShopDao();
     ShopBeans shopName = shop.findByShop(name);
     if (shopName == null){
       ShopDao Shop = new ShopDao();
       Shop.insertShop(name,Place);
       request.setAttribute("postMsg", "店舗が見つかりました");

      
       response.sendRedirect("ShopPostAdd");     
      }else {
    
      response.sendRedirect("ShopPostAdd");     
  }
}
}