package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.ShopPostDao;

/**
 * Servlet implementation class ShopTreadDelete
 */
@WebServlet("/ShopPostDelete")
public class ShopPostDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopPostDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      if (user == null) {
        
        request.setAttribute("errMsg", "ログインしてください。");
        response.sendRedirect("Login");
        return;
      }
      int postId= Integer.parseInt(request.getParameter("postId"));
      request.setAttribute("postId",postId);
      
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_delete.jsp");
      dispatcher.forward(request, response);
    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
/*	  if(!(name.equals(shopName))){
	       request.setAttribute("errMsg", "登録した店舗名を入力してください");

	       RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_add.jsp");
	       dispatcher.forward(request, response);
	       return;
	     }*/
	  String User = request.getParameter("user");
	  int postId= Integer.parseInt(request.getParameter("postId"));
	 
	  ShopPostDao shopPostDao = new ShopPostDao();
      shopPostDao.deletePost(postId);
      request.setAttribute("postId",postId);
      request.setAttribute("user",User);
      request.setAttribute("postMsg", "投稿を削除しました。");
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_delete.jsp");
      dispatcher.forward(request, response);
//      response.sendRedirect("UserPostList");
	}

}
