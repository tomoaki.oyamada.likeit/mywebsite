package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.ShopPostDao;

/**
 * Servlet implementation class ShopTreadWatch
 */
@WebServlet("/ShopPostWatch")
public class ShopPostWatch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopPostWatch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    
      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");
      int postId = Integer.parseInt(request.getParameter("postId"));
     
// int shopId = Integer.parseInt(request.getParameter("shopId"));
      ShopPostDao shoppost= new ShopPostDao();
      ShopPostDataBeans targetPost =shoppost.findByPostld(postId);
      request.setAttribute("postId",targetPost); 
      ShopPostDataBeans userName = shoppost.findByPostUserName(postId);
      request.setAttribute("user",userName); 
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_post_watch.jsp");
      dispatcher.forward(request, response);
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
