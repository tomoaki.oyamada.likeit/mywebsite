package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserDataBeans;
import dao.UserDao;
import passwordEnco.PasswordEncorder;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");
      request.setAttribute("user", user);
    
      if (user == null) {
        
        request.setAttribute("errMsg", "ログインしてください。");
        response.sendRedirect("Login");
        return;
      }
   
      
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
      dispatcher.forward(request, response);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("UTF-8");

	      HttpSession session = request.getSession();
	      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");
	      int id = user.getId();
	      String password = request.getParameter("password");
	      String password2 = request.getParameter("password-confirm");
	      String name = request.getParameter("name");
	      String Age =request.getParameter("age");
	      String Gender = request.getParameter("gender");
	      int age=Integer.parseInt(Age);
	      int gender=Integer.parseInt(Gender);
	     
	      if ((!(password.equals(password2))) || password.equals("")
	          || password2.equals("") || name.equals("") ||Age== null||Gender==null) {
	        request.setAttribute("errMsg", "入力された内容は正しくありません");
	        request.setAttribute("inputName", name);
	        request.setAttribute("inputAge", age);
	        
	        
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
	        dispatcher.forward(request, response);
	        return;
	    }
	      
	      UserDao userDao = new UserDao();

	        PasswordEncorder pe = new PasswordEncorder();
	        String Password = pe.encordPassword(password);

	      userDao.update( Password, name, age, gender,id);
	      request.setAttribute("userAddMsg", "登録が完了しました");
	      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
	      dispatcher.forward(request, response);
	    }	     
//	      response.sendRedirect("Top");
	}


