package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;
import beans.UserDataBeans;
import dao.AverageDao;
import dao.ShopDao;
import dao.ShopPostDao;
//このページいらない？　点数だけ平均出して、トップでランキングにする。
//トップからはショップリスtpに行く。
/**
 * Servlet implementation class ShopSearchResult
 */
@WebServlet("/ShopSearchResult")
public class ShopSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopSearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @return 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserDataBeans user= (UserDataBeans) session.getAttribute("userInfo");

      
      int shopId = Integer.parseInt(request.getParameter("shopId"));
  
      ShopPostDao shopPostData = new ShopPostDao();
      ShopDao shopData = new ShopDao();
      ShopPostDataBeans shop = shopPostData.findByShopld(shopId);
      ShopDataBeans average = shopData.findByShopld(shopId);
      request.setAttribute("shopId", shopId);
      request.setAttribute("average", average);
      request.setAttribute("shop", shop);
//      ShopPostDataBeans postData =  shopPostData.findByPostld(postId);
//      int shopId = postData.getShopId();
      request.setAttribute("shopId", shopId);
      
      ShopDataBeans shopAverage = shopData.findByShopld(shopId);
      request.setAttribute("shopAverages",shopAverage.getScoreAverage()+","+ shopAverage.getServiceAverage()+","+shopAverage.getAtomsphereAverage()+","+shopAverage.getCleanAverage()+","+shopAverage.getVolumeAverage());
//      ","+shopAverage.getPriceAverage()+
           /* ShopPostDataBeans place=shopPostList.get(5);
      ShopPostDataBeans shopName=shopPostList.get(0);
   */   
   /*  AverageDao average = new AverageDao();
      double scoreAverage= average.getScoreAverage(shopId);
      double service= average.getService(shopId)     double volume= average.getVolume(shopId);
      double price= average.getPrice(shopId);
      int taste = average.getTaste(shopId);
   */
// ShopDao shopDao = new ShopDao();
// ShopDao ShopBeans = shopDao.insertPostAverage(shopId, shopName, scoreAverage, service, volume, price, taste, place );
      
     RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shop_search_result.jsp");
     dispatcher.forward(request, response);
     }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
