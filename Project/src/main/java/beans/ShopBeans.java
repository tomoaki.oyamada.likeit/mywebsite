package beans;

import java.io.Serializable;

public class ShopBeans implements Serializable{
  private int shopId;
  private String shopName;
  private String file;
  private String place;
  private byte[] img;
  
  public byte[] getImg() {
    return img;
  }



  public void setImg(byte[] img) {
    this.img = img;
  }



  public ShopBeans (int shopId , String shopName,String file, String place
  ) {
    
    this.shopId= shopId;
    this.shopName=shopName;
    this.file=file;
    this.place=place;
    
}

  

  public ShopBeans(int shopId, String shopName, byte[] img, String place) {
    this.shopId= shopId;
    this.shopName=shopName;
    this.img=img;
    this.place=place;
  }



  public ShopBeans(byte[] img) {
    this.img=img;
  }



  public int getShopId() {
    return shopId;
  }

  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public String getPlace() {
    return place;
  }

  public void setPlace(String place) {
    this.place = place;
  }
}