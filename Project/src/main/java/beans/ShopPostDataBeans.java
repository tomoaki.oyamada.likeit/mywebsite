package beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class ShopPostDataBeans implements Serializable {

     private int shopId;
     private String shopName;
     private int score;
     private int service;
     private int volume;
     private int price;
     private int taste;
     private int atomsphere;
     private int clean;
     private String place;
     private int id;
     private String file;
     private int postId;
     private String body;
     private String userName;
     

     // 全てのデータをセットするコンストラクタ
     public ShopPostDataBeans(int shopId,String shopName, int score, int service,int volume, int price,int atomsphere,
    int clean ,int taste, String _place,int id,String file,int postId,String body) {
       this.id = id;
       this.shopName=shopName;
       this.shopId = shopId;
       this.score = score;
       this.service = service;
       this.volume = volume;
       this.price = price;
       this.taste=taste;
       this.place = _place;
       this.file = file;
       this.postId=postId;
       this.body= body;
       this.atomsphere=atomsphere;
       this.clean=clean;
     }
    public ShopPostDataBeans(int shopId, String shopName, int score, int service, int volume,
        int atomsphere, int clean, int price, int taste, String place, int id, String file,
        int postId, String body, String userName) {
      this.id = id;
      this.shopName=shopName;
      this.shopId = shopId;
      this.score = score;
      this.service = service;
      this.volume = volume;
      this.price = price;
      this.taste=taste;
      this.place = place;
      this.file = file;
      this.postId=postId;
      this.body= body;
      this.atomsphere=atomsphere;
      this.clean=clean;
      this.userName=userName;
    }
    public ShopPostDataBeans(String userName, int postId, int id) {
      this.userName=userName;
      this.postId=postId;
      this.id = id;
    }
    public ShopPostDataBeans(int shopId, String shopName) {
      this.shopName=shopName;
      this.shopId = shopId;
    }
    public String getShopName() {
      return shopName;
    }
    public void setShopName(String shopName) {
      this.shopName = shopName;
    }
    public int getPostId() {
      return postId;
    }
    public void setPostId(int postId) {
      this.postId = postId;
    }
    public int getTaste() {
      return taste;
    }
    public void setTaste(int taste) {
      this.taste = taste;
    }
    public String getBody() {
      return body;
    }
    public void setBody(String body) {
      this.body = body;
    }
    public int getShopId() {
      return shopId;
    }
    public void setShopId(int shopId) {
      this.shopId = shopId;
    }
    public int getScore() {
      return score;
    }
    public void setScore(int score) {
      this.score = score;
    }
    public int getService() {
      return service;
    }
    public void setService(int service) {
      this.service = service;
    }
    public int getVolume() {
      return volume;
    }
    public void setVolume(int volume) {
      this.volume = volume;
    }
    public int getPrice() {
      return price;
    }
    public void setPrice(int price) {
      this.price = price;
    }
    public String getPlace() {
      return place;
    }
    public void setPlace(String place) {
      this.place = place;
    }
    public int getId() {
      return id;
    }
    public void setId(int id) {
      this.id = id;
    }
    
    public int getAtomsphere() {
      return atomsphere;
    }
    public void setAtomsphere(int atomsphere) {
      this.atomsphere = atomsphere;
    }
    public int getClean() {
      return clean;
    }
    public void setClean(int clean) {
      this.clean = clean;
    }
    public String getFile() {
      return file;
    }
    public void setFile(String file) {
      this.file = file;
    }
    public String getUserName() {
      return userName;
    }
    public void setUserName(String userName) {
      this.userName = userName;
    }
   
   
}
