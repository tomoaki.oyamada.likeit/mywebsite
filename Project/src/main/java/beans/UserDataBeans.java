package beans;

import java.io.Serializable;


/**
 * ユーザー
 * @author d-yamaguchi
 *
 */
public class UserDataBeans implements Serializable {
  
    private int id;
    private boolean isAdmin;
    private String loginId;
	private String name;
	private int age ;
	private int gender;
	private String password;
	
	public UserDataBeans(int id,  String name,int age,String password, boolean isAdimn,int gender,String loginId) {
	    this.id = id;
	    this.loginId = loginId;
	    this.name = name;
	    this.age = age;
	    this.password = password;
	    this.isAdmin = isAdimn;
	    this.gender = gender;
	}
	
	public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public int getAge() {
    return age;
  }
  public void setAge(int age) {
    this.age = age;
  }
  public int getGender() {
    return gender;
  }
  public void setGender(int gender) {
    this.gender = gender;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public boolean isAdmin() {
    return isAdmin;
  }
  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }
  public String getLoginId() {
    return loginId;
  }
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }
 
    }
