package beans;

import java.io.Serializable;

public class ShopDataBeans implements Serializable{
  private int shopId;
  private int id;
  private int scoreAverage;
  private int serviceAverage;
  private int atomsphereAverage;
  private int cleanAverage;
  private  int volumeAverage;
  private  int priceAverage;
 

  


  public ShopDataBeans(int shopId, int id,int scoreAverage, int serviceAverage, int atomsphereAverage,int cleanAverage,int volumeAverage,int priceAverage) {
    this.id=id;
    this.shopId = shopId;
    this.scoreAverage = scoreAverage;
    this.atomsphereAverage=atomsphereAverage;
    this.cleanAverage=cleanAverage;
    this.priceAverage=priceAverage;
        this.volumeAverage=volumeAverage;
        this.serviceAverage=serviceAverage;
    
    
  }
  
  public ShopDataBeans(int shopId, int scoreAverage) {
    this.shopId = shopId;
    this.scoreAverage = scoreAverage;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getShopId() {
    return shopId;
  }
  public void setShopId(int shopId) {
    this.shopId = shopId;
  }

  public int getScoreAverage() {
    return scoreAverage;
  }
  public void setScoreAverage(int scoreAverage) {
    this.scoreAverage = scoreAverage;
  }

  public int getServiceAverage() {
    return serviceAverage;
  }

  public void setServiceAverage(int serviceAverage) {
    this.serviceAverage = serviceAverage;
  }

  public int getAtomsphereAverage() {
    return atomsphereAverage;
  }

  public void setAtomsphereAverage(int atomsphereAverage) {
    this.atomsphereAverage = atomsphereAverage;
  }

  public int getCleanAverage() {
    return cleanAverage;
  }

  public void setCleanAverage(int cleanAverage) {
    this.cleanAverage = cleanAverage;
  }

  public int getVolumeAverage() {
    return volumeAverage;
  }

  public void setVolumeAverage(int volumeAverage) {
    this.volumeAverage = volumeAverage;
  }

  public int getPriceAverage() {
    return priceAverage;
  }

  public void setPriceAverage(int priceAverage) {
    this.priceAverage = priceAverage;
  }
  
}
