package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import base.DBManager;
import beans.ShopBeans;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;

public class AverageDao {

  public void insertAverage(int shopId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql2 =
          "insert into shop_data (score_average, price_average, service_average,atomsphere_average,clean_average,volume_average,id) VALUES "
          + "(select avg(score) from post_data),(select avg(price) from post_data),(select avg(service) from  post_data),"
          + "(select avg(atomsphere) from post_data),(select avg(clean) from post_data),(select avg(volume) from post_data),(select max(id) from shop)";
    
      Statement Stmt=conn.createStatement();
      Stmt.executeUpdate(sql2);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    }
  
  public void updateAverage(int shopId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
           " update shop_data set score_average=(select avg(score) from post_data), price_average=(select avg(price) from post_data), "
              + "service_average=(select avg(service) from  post_data),atomsphere_average=(select avg(atomsphere) from post_data),"
              + "clean_average=(select avg(clean) from post_data),volume_average=(select avg(volume) from post_data)";
         
      Statement Stmt=conn.createStatement();
      Stmt.executeUpdate(sql);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    }
  /*
 public  ShopDataBeans findShopRank1() {
      Connection conn = null;
     try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM (select score_average from shop_data  ORDER BY score_average DESC)as rank WHERE 1 <= rank ";
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();


        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        int scoreAverage = rs.getInt("score_average");
    
      return new ShopDataBeans(_shopId,scoreAverage);

 } catch (SQLException e) {
   e.printStackTrace();
   return null;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return null;
     }
   }
 }
 }
 
 public  ShopDataBeans findShopRank2() {
      Connection conn = null;
     try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM shop_data WHERE 2 <= score_average < 1  ORDER BY score_average DESC";
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();


        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        int scoreAverage = rs.getInt("score_average");
       

        return new ShopDataBeans(_shopId,scoreAverage);

 } catch (SQLException e) {
   e.printStackTrace();
   return null;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return null;
     }
   }
 }
 }
 
 public  ShopDataBeans findShopRank3() {
      Connection conn = null;
     try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql ="SELECT * FROM shop_data WHERE 3 <= score_average < 2 ORDER BY score_average DESC";
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();


        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        int scoreAverage = rs.getInt("score_average");
     
       

        return new ShopDataBeans(_shopId,scoreAverage);

 } catch (SQLException e) {
   e.printStackTrace();
   return null;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return null;
     }
   }
 }
 }  
  
 */ 
 public  ShopDataBeans findShopRank1() {
      Connection conn = null;
     try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "select * from (select a.id,a.shop_id,a.score_average,(select count(b.score_average) + 1 from shop_data as b where a.score_average < b.score_average ) as rank, a.service_average,a.atomsphere_average,a.clean_average,a.volume_average, a.price_average from shop_data as a order by rank desc)as r where rank = 1";
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();


        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        int scoreAverage = rs.getInt("score_average");
    
      return new ShopDataBeans(_shopId,scoreAverage);

 } catch (SQLException e) {
   e.printStackTrace();
   return null;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return null;
     }
   }
 }
 }
 
 public  ShopDataBeans findShopRank2() {
      Connection conn = null;
     try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "select * from (select a.id,a.shop_id,a.score_average,(select count(b.score_average) + 1 from shop_data as b where a.score_average < b.score_average ) as rank, a.service_average,a.atomsphere_average,a.clean_average,a.volume_average, a.price_average from shop_data as a order by rank desc)as r where rank = 2";
            
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();


        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        int scoreAverage = rs.getInt("score_average");
       

        return new ShopDataBeans(_shopId,scoreAverage);

 } catch (SQLException e) {
   e.printStackTrace();
   return null;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return null;
     }
   }
 }
 }
 
 public  ShopDataBeans findShopRank3() {
      Connection conn = null;
     try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "select * from (select a.id,a.shop_id,a.score_average,(select count(b.score_average) + 1 from shop_data as b where a.score_average < b.score_average ) as rank, a.service_average,a.atomsphere_average,a.clean_average,a.volume_average, a.price_average from shop_data as a order by rank desc)as r where rank = 3";
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();


        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        int scoreAverage = rs.getInt("score_average");
     
       

        return new ShopDataBeans(_shopId,scoreAverage);

 } catch (SQLException e) {
   e.printStackTrace();
   return null;
 } finally {
   // データベース切断
   if (conn != null) {
     try {
       conn.close();
     } catch (SQLException e) {
       e.printStackTrace();
       return null;
     }
   }
 }
 }
 public void insertPostAverage(int shopId,String shopName ,int scoreAverage, String place,String file){
   Connection conn = null;
   try {
     // データベースへ接続
     conn = DBManager.getConnection();

     // SELECT文を準備
     String sql =
         "INSERT INTO shop_data (shop_id,shop_name,score_average,file,place) VALUES (?,?,?,?,?)";

     // SELECTを実行し、結果表を取得
     PreparedStatement stmt = conn.prepareStatement(sql);
     stmt.setString(2, shopName);
     stmt.setInt(3, scoreAverage);
     stmt.setString(5, place);
     stmt.setString(4, file);
     stmt.setInt(1, shopId);
     stmt.executeUpdate();


   } catch (SQLException e) {
     e.printStackTrace();

   } finally {
     // データベース切断
     if (conn != null) {
       try {
         conn.close();
       } catch (SQLException e) {
         e.printStackTrace();
       }
     }
   }
 }
 
 
  public  double getScoreAverage(int shopId) {
    ShopPostDao shop= new ShopPostDao();
    List<ShopPostDataBeans> shopPostList= shop.findShopAll(shopId);
    ShopPostDataBeans score= shopPostList.get(3);
    Map<ShopPostDataBeans, List<Integer>> map = new HashMap<>();
    double scoreAverage = 0;
  for (int i = 0; i <shopPostList.size(); i++) {
      ShopPostDataBeans _score = shopPostList.get(i);
      List<Integer> scoresize = map.get(_score);
      if (scoresize == null) {
          scoresize = new ArrayList<Integer>();
          map.put(score, scoresize);
      }
      scoresize.add(i);
      int sum = 0; 
      for ( Integer Score: scoresize) sum += Score;
              
      //平均値を取得
     scoreAverage = sum / scoresize.size();
  }
  return scoreAverage;
  }   
}