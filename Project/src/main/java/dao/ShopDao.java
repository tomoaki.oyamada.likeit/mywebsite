package dao;


import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import base.DBManager;
import beans.ShopBeans;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;

public class ShopDao {

  
  public ShopDataBeans findByShopld(int shopId) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT*FROM shop_data WHERE shop_id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1,shopId);
      ResultSet rs = pStmt.executeQuery();
     
      if (!rs.next()) {
        return null;
      }
      int _shopId=rs.getInt("shop_id");
      int id= rs.getInt("id");
      int scoreAverage = rs.getInt("score_average");
      int serviceAverage=rs.getInt("service_average");
      int atomsphereAverage=rs.getInt("atomsphere_average");
      int cleanAverage=rs.getInt("clean_average");
      int volumeAverage=rs.getInt("volume_average");
      int priceAverage=rs.getInt("price_average");
     


      return new ShopDataBeans(_shopId,id,scoreAverage ,serviceAverage,atomsphereAverage,cleanAverage,volumeAverage,priceAverage);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
}
  public List<ShopDataBeans> findAll(int shopId) {
    Connection conn = null;
    List<ShopDataBeans> shopList = new ArrayList<ShopDataBeans>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM shop_data where shop_id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1,shopId);
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int _shopId=rs.getInt("shop_id");
        int id= rs.getInt("id");
        int scoreAverage = rs.getInt("score_average");
        int serviceAverage=rs.getInt("service_average");
        int atomsphereAverage=rs.getInt("atomsphere_average");
        int cleanAverage=rs.getInt("clean_average");
        int volumeAverage=rs.getInt("volume_average");
        int priceAverage=rs.getInt("price_average");
       

        ShopDataBeans shop =
            new ShopDataBeans(_shopId,id,scoreAverage ,serviceAverage,atomsphereAverage,cleanAverage,volumeAverage,priceAverage);

        shopList.add(shop);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return shopList;
  }
//    比較するためにサーブレッドで全部Stringに返還すry
    public static List<ShopPostDataBeans> searchA(String shopName, String lowPrice,String maxPrice , String place,String atomsphere,String clean,String service,String volume,String taste) {
      Connection conn = null;
      List<ShopPostDataBeans> shopList = new ArrayList<ShopPostDataBeans>();
      try {
        // データベースへ接続
        conn = DBManager.getConnection();
         
        String sql =
            "select * from post_data";
        
        StringBuilder stringBuilder = new StringBuilder(sql);
     
        if(shopName==""&&lowPrice==""&&maxPrice==""&&place==""&&volume==""&&service==""&&atomsphere==""&&clean==""){
          stringBuilder.append(" where taste=?");

        } else if(shopName==""&&lowPrice==""&&maxPrice==""&&place==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
            stringBuilder.append(" where volume=?");
        } else if(shopName==""&&lowPrice==""&&maxPrice==""&&volume==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
          stringBuilder.append(" where place=?");
        
        }else if (lowPrice==""&&maxPrice==""&&place==""&&volume==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
          stringBuilder.append(" where shop_name Like ?");
        }else if(place==""&&shopName==""&&volume==""&&taste==""&&lowPrice==""&&maxPrice==""&&atomsphere==""&&clean==""){
          stringBuilder.append(" where service= ?"); 
        }else if(place==""&&shopName==""&&volume==""&&taste==""&&lowPrice==""&&maxPrice==""&&service==""&&atomsphere==""){
          stringBuilder.append(" where clean= ?"); 
        }else if(place==""&&shopName==""&&volume==""&&taste==""&&lowPrice==""&&maxPrice==""&&service==""&&clean==""){
          stringBuilder.append(" where atomsphere= ?"); 
        }else if(place==""&&shopName==""&&volume==""&&taste==""&&lowPrice==""&&maxPrice==""&&atomsphere==""&&clean==""){
          stringBuilder.append(" where service= ?"); 
          
        }else if(place==""&&shopName==""&&volume==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
          stringBuilder.append(" where price between ? and ?"); 
        }else{
          stringBuilder.append(" where place=? AND shop_name like ? and volume=? and taste=? and service=? And price between ? and ?");
        }
        
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
        
        if(shopName==""&&lowPrice==""&&maxPrice==""&&place==""&&volume==""&&service==""&&atomsphere==""&&clean==""){
          pStmt.setString(1, taste);
       } else if(shopName==""&&lowPrice==""&&maxPrice==""&&place==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
          pStmt.setString(1, volume);
      } else if(shopName==""&&lowPrice==""&&maxPrice==""&&volume==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
        pStmt.setString(1, place);      pStmt.setString(1, taste);
      } else if(shopName==""&&lowPrice==""&&maxPrice==""&&place==""&&taste==""&&service==""&&atomsphere==""&&volume==""){
        pStmt.setString(1, clean);
     } else if(shopName==""&&lowPrice==""&&maxPrice==""&&volume==""&&taste==""&&service==""&&place==""&&clean==""){
      pStmt.setString(1,atomsphere );
      } else if(shopName==""&&lowPrice==""&&maxPrice==""&&volume==""&&taste==""&&place==""&&atomsphere==""&&clean==""){
        pStmt.setString(1, service);
      }else if (lowPrice==""&&maxPrice==""&&place==""&&volume==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
        
          pStmt.setString(1, "%" + shopName + "%");
        
      }else if(place==""&&shopName==""&&volume==""&&taste==""&&service==""&&atomsphere==""&&clean==""){
          pStmt.setString(1, lowPrice);
          pStmt.setString(2, maxPrice);
        }else {
         pStmt.setString(1, place);
         pStmt.setString(2, "%" + shopName + "%");
         pStmt.setString(3, volume);
         pStmt.setString(4,taste);
         pStmt.setString(5, service);
         pStmt.setString(6, lowPrice);
         pStmt.setString(7,maxPrice);
        } 
        
         ResultSet rs = pStmt.executeQuery();

        while (rs.next()) {
          int shopId = rs.getInt("shop_id");
          String _shopName= rs.getString("shop_name");
          int score = rs.getInt("score");
          int _service = rs.getInt("service");
          int _volume = rs.getInt("volume");
          int price = rs.getInt("price");
          int _atomsphere=rs.getInt("atomsphere");
          int _clean=rs.getInt("clean");
          int _taste= rs.getInt("taste");
          String _place = rs.getString("place");
          String file = rs.getString("file");
          String body = rs.getString("body");
          int id = rs.getInt("id");
          int postId = rs.getInt("post_id");
          
          ShopPostDataBeans shop =
              new ShopPostDataBeans(shopId,_shopName,score ,_service,_volume ,_atomsphere,_clean,price , _taste, _place, id,file,postId,body);

          shopList.add(shop);
        }
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
          return null;
          }
        }
      }
      return shopList;
    }
    public List<ShopPostDataBeans> search(String shopName, String lowPrice,String maxPrice , String place) {
      Connection conn = null;
      List<ShopPostDataBeans> shopList = new ArrayList<ShopPostDataBeans>();
      try {
        // データベースへ接続
        conn = DBManager.getConnection();
         
        String sql =
            "select * from post_data as a inner join shop as b on a.shop_id = b.shop_id and a.shop_name = b.shop_name";
//        kokokara
        
        StringBuilder stringBuilder = new StringBuilder(sql);
     
        if(shopName==""&&lowPrice==""&&maxPrice==""){
          stringBuilder.append(" where b.place=?");
        
        }else if (lowPrice==""&&maxPrice==""&&place==""){
          stringBuilder.append(" where a.shop_name Like ?");

        }else if(place==""&&shopName==""){
          stringBuilder.append(" where a.price between ? and ?"); 
        
        }else if(shopName==""){
          stringBuilder.append(" where b.place = ? and a.price between ? and ?"); 
        
         }else if (lowPrice==""&&maxPrice==""){
           stringBuilder.append(" where a.shop_name Like ? and b.place=?");
           
         }else if(place==""){
           stringBuilder.append(" where a.shop_name = ? a.price between ? and ?"); 
         
      
           
        }else{
          stringBuilder.append(" where b..place=? AND a.shop_name like ? And a.price between ? and ?");
        }
        
        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
        
      if(shopName==""&&lowPrice==""&&maxPrice==""){
        pStmt.setString(1, place);     
      
      }else if (lowPrice==""&&maxPrice==""&&place==""){
        
          pStmt.setString(1, "%" + shopName + "%");
        
      }else if(place==""&&shopName==""){
          pStmt.setString(1, lowPrice);
          pStmt.setString(2, maxPrice);
          
         }else if(shopName==""){
             pStmt.setString(1, place); 
             pStmt.setString(2, lowPrice);
             pStmt.setString(3, maxPrice);
             
         }else if(lowPrice==""&&maxPrice==""){
             pStmt.setString(1, shopName);
             pStmt.setString(2, place);
             
         }else if(place==""){
           pStmt.setString(1, shopName); 
           pStmt.setString(2, lowPrice);
           pStmt.setString(3, maxPrice);
        }else {
         pStmt.setString(1, place);
         pStmt.setString(2, "%" + shopName + "%");
         pStmt.setString(3, lowPrice);
         pStmt.setString(4,maxPrice);
        } 
        
         ResultSet rs = pStmt.executeQuery();

        while (rs.next()) {
          int shopId = rs.getInt("shop_id");
          String _shopName= rs.getString("shop_name");
          int score = rs.getInt("score");
          int _service = rs.getInt("service");
          int _volume = rs.getInt("volume");
          int price = rs.getInt("price");
          int _atomsphere=rs.getInt("atomsphere");
          int _clean=rs.getInt("clean");
          int _taste= rs.getInt("taste");
          String _place = rs.getString("place");
          String file = rs.getString("file");
          String body = rs.getString("body");
          int id = rs.getInt("id");
          int postId = rs.getInt("post_id");
          
          ShopPostDataBeans shop =
              new ShopPostDataBeans(shopId,_shopName,score ,_service,_volume ,_atomsphere,_clean,price , _taste, _place, id,file,postId,body);

          shopList.add(shop);
        }
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
          return null;
          }
        }
      }
      return shopList;
    }

public void insertShop(String shopName,String place) {
  Connection conn = null;
  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql =
        "INSERT INTO shop (shop_name ,place)VALUES (?,?)";


    PreparedStatement stmt = conn.prepareStatement(sql);
    stmt.setString(1, shopName);
    stmt.setString(2, place);
    stmt.executeUpdate();
 
     String sql2 =
        "insert into shop_data (shop_id) VALUES ((select max(shop_id) from shop))";
  
    Statement Stmt=conn.createStatement();
    Stmt.executeUpdate(sql2);

  } catch (SQLException e) {
    e.printStackTrace();

  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
  }
  public ShopBeans findByShop(String shopName) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM shop WHERE shop_name = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1,shopName);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int _shopId=rs.getInt("shop_id");
      String _shopName= rs.getString("shop_name");
      String place=rs.getString("place");
      String file = rs.getString("file");
    
      return new ShopBeans(_shopId,_shopName,file,place);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
    public ShopBeans findByShop2(int shopId) {
      Connection conn = null;

      try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM shop WHERE shop_id = ?";

        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1,shopId);
        ResultSet rs = pStmt.executeQuery();

        if (!rs.next()) {
          return null;
        }
        int _shopId=rs.getInt("shop_id");
        String _shopName= rs.getString("shop_name");
        String place=rs.getString("place");
        String file = rs.getString("file");
        
        return new ShopBeans(_shopId,_shopName,file,place);



      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
      }
} public ShopDataBeans findShopScoreMax( ) {
  Connection conn = null;

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "select max (score_average) from shop_data"; 

    // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    ResultSet rs = pStmt.executeQuery();

    if (!rs.next()) {
      return null;
    }
    int _shopId=rs.getInt("shop_id");
    int id= rs.getInt("id");
    int scoreAverage = rs.getInt("score_average");
    int serviceAverage=rs.getInt("service_average");
    int atomsphereAverage=rs.getInt("atomsphere_average");
    int cleanAverage=rs.getInt("clean_average");
    int volumeAverage=rs.getInt("volume_average");
    int priceAverage=rs.getInt("price_average");
   


    return new ShopDataBeans(_shopId,id,scoreAverage ,serviceAverage,atomsphereAverage,cleanAverage,volumeAverage,priceAverage);



  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
}
 /* public ShopBeans findShopMaxId( ) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "select max(shop_id) as shop_id from shop"; 

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int shopId=rs.getInt("shop_id");
      
      return new ShopBeans(shopId);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
         
        }
      }
    }
  }*/
public void insertShopfile(String file,int shop_id) {
  Connection conn = null;

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql =
        " update  shop set file = ? where shop_id = ?";


    // SELECTを実行し、結果表を取得
    PreparedStatement stmt = conn.prepareStatement(sql);
  
    stmt.setString(1, file);
    stmt.setInt(2, shop_id);
    stmt.executeUpdate();


  } catch (SQLException e) {
    e.printStackTrace();

  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
  }
public void insertShopImg(byte[] img, int shop_id) {
  Connection conn = null;

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql =
        " update  shop set img = ? where shop_id = ?";


    // SELECTを実行し、結果表を取得
    PreparedStatement stmt = conn.prepareStatement(sql);
  
    stmt.setBinaryStream(1, new ByteArrayInputStream(img));
    stmt.setInt(2, shop_id);
    stmt.executeUpdate();


  } catch (SQLException e) {
    e.printStackTrace();

  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  
}

  
}
public ShopBeans findByShop_(int shopId) {
  Connection conn = null;

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT * FROM shop WHERE shop_id = ?";

    // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setInt(1,shopId);
    ResultSet rs = pStmt.executeQuery();

    if (!rs.next()) {
      return null;
    }
    int _shopId=rs.getInt("shop_id");
    String _shopName= rs.getString("shop_name");
    String place=rs.getString("place");
    byte[] img = rs.getBytes("img");
    
    return new ShopBeans(_shopId,_shopName,img,place);



  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
}
}