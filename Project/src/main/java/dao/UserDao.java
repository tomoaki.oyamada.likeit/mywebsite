package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.UserDataBeans;


public class UserDao {
  
  public void insertUser(String loginId,String password,String name, int age,int gender) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO user(login_id,password,name,age,gender)VALUES(?,?,?,?,?)";

      // SELECTを実行し、結果表を取得
      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setString(1, loginId);
      stmt.setString(2, password);
      stmt.setString(3, name);
      stmt.setInt(4, age);
      stmt.setInt(5, gender);
      stmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


  public UserDataBeans findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name");
      int age = rs.getInt("age");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      int gender = rs.getInt("gender");
      String _loginId=rs.getString("login_id");
     return new UserDataBeans(id, name, age, _password, isAdmin, gender,_loginId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


public List<UserDataBeans> findAll() {
  Connection conn = null;
  List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    // TODO: 未実装：管理者以外を取得するようSQLを変更する
    String sql = "SELECT * FROM user WHERE is_admin = false";

    // SELECTを実行し、結果表を取得
    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery(sql);

    // 結果表に格納されたレコードの内容を
    // Userインスタンスに設定し、ArrayListインスタンスに追加
    while (rs.next()) {
      int id = rs.getInt("id");
      String name = rs.getString("name");
      int age = rs.getInt("age");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      int gender = rs.getInt("gender");
      String _loginId=rs.getString("login_id");
      UserDataBeans user =
          new UserDataBeans(id, name, age, _password, isAdmin, gender,_loginId);

      userList.add(user);
    }
  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
  return userList;
}

public UserDataBeans findByld(int id) {
Connection conn = null;

try {
  // データベースへ接続
  conn = DBManager.getConnection();

  // SELECT文を準備
  String sql = "SELECT*FROM user WHERE id = ?";

  // SELECTを実行し、結果表を取得
  PreparedStatement pStmt = conn.prepareStatement(sql);
  pStmt.setInt(1, id);
  ResultSet rs = pStmt.executeQuery();

  if (!rs.next()) {
    return null;
  }
  int Id = rs.getInt("id");
  String name = rs.getString("name");
  int age = rs.getInt("age");
  String _password = rs.getString("password");
  boolean isAdmin = rs.getBoolean("is_admin");
  int gender = rs.getInt("gender");
  String _loginId=rs.getString("login_id");
  return new UserDataBeans(Id, name, age, _password, isAdmin, gender,_loginId);


} catch (SQLException e) {
  e.printStackTrace();
  return null;
} finally {
  // データベース切断
  if (conn != null) {
    try {
      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
}


public void update(String password,String name,int age,int gender,int id) {
  Connection conn = null;

  try{ 
    conn = DBManager.getConnection();
    
    String sql = "UPDATE user SET password=?,name=?,age=?,gender=? WHERE id=?";

    PreparedStatement stmt = conn.prepareStatement(sql);
    stmt.setString(1, password);
    stmt.setString(2, name);
    stmt.setInt(3, age);
    stmt.setInt(4, gender);
    stmt.setInt(5, id);
    stmt.executeUpdate();
   
  } catch (SQLException e) {
    e.printStackTrace();

  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
public UserDataBeans findByuser(String name) {
Connection conn = null;

try {
  // データベースへ接続
  conn = DBManager.getConnection();

  // SELECT文を準備
  String sql = "SELECT*FROM user WHERE name = ?";

  // SELECTを実行し、結果表を取得
  PreparedStatement pStmt = conn.prepareStatement(sql);
  pStmt.setString(1, name);
  ResultSet rs = pStmt.executeQuery();

  if (!rs.next()) {
    return null;
  }
  int Id = rs.getInt("id");
  String _name = rs.getString("name");
  int age = rs.getInt("age");
  String _password = rs.getString("password");
  boolean isAdmin = rs.getBoolean("is_admin");
  int gender = rs.getInt("gender");
  String _loginId=rs.getString("login_id");
  return new UserDataBeans(Id, _name, age, _password, isAdmin, gender,_loginId);


} catch (SQLException e) {
  e.printStackTrace();
  return null;
} finally {
  // データベース切断
  if (conn != null) {
    try {
      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
}
}
