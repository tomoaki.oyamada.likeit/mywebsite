package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.ShopDataBeans;
import beans.ShopPostDataBeans;

public class ShopPostDao {
  
  public void insertPost(String shopName,int score,int price,int clean, int service,int volume ,int taste,int atomsphere,String body,int id,int shop_id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO post_data (shop_name,score,price,clean,service,volume,taste,atomsphere,body,id,shop_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

      // SELECTを実行し、結果表を取得
      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setString(1, shopName);
      stmt.setInt(2, score);
      stmt.setInt(5, service);
      stmt.setInt(6, volume);
      stmt.setInt(3,price);
      stmt.setInt(4, clean);
      stmt.setInt(7, taste);
      stmt.setInt(8,atomsphere);
      stmt.setString(9,body);
      stmt.setInt(10,id);
      stmt.setInt(11, shop_id);
      stmt.executeUpdate();
      
   String sql2 ="update shop_data as A set A.score_average = (select avg(score) from post_data  where shop_id = A.shop_id ), A.price_average = (select avg(price) from post_data where shop_id = A.shop_id), A.service_average = (select avg(service) from post_data where shop_id = A.shop_id),A.atomsphere_average = (select avg(atomsphere) from post_data where shop_id = A.shop_id),A.clean_average = (select avg(clean) from post_data where shop_id = A.shop_id),A.volume_average = (select avg(volume) from post_data where shop_id = A.shop_id )where shop_id = ?";
     
     PreparedStatement Stmt = conn.prepareStatement(sql2);
     Stmt.setInt(1, shop_id);
     Stmt.executeUpdate();
    
    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

// ここはどうするか後で決める
//IDから投稿を探す
public ShopPostDataBeans findByPostld(int postId) {
Connection conn = null;

try {
  // データベースへ接続
  conn = DBManager.getConnection();

  // SELECT文を準備
  String sql = "SELECT*FROM ( post_data ) inner join shop on post_data.shop_id = shop.shop_id WHERE post_id = ?";

  // SELECTを実行し、結果表を取得
  PreparedStatement pStmt = conn.prepareStatement(sql);
  pStmt.setInt(1,postId);
  ResultSet rs = pStmt.executeQuery();

    if (!rs.next()) {
      return null;
    }

    int shopId = rs.getInt("shop_id");
    String shopName= rs.getString("shop_name");
    int _postId = rs.getInt("post_id"); 
    int score = rs.getInt("score");
    int service = rs.getInt("service");
    int volume = rs.getInt("volume"); 
    int atomsphere = rs.getInt("atomsphere");
    int clean = rs.getInt("clean"); 
    int price = rs.getInt("price");
    int taste = rs.getInt("taste");
    int id = rs.getInt("id");
    String place = rs.getString("place");
    String file = rs.getString("file");
    String body=rs.getString("body");
   
  return new ShopPostDataBeans(shopId,shopName, score, service, volume,price, atomsphere,clean, taste,  place,
      id,file,_postId,body);


} catch (SQLException e) {
  e.printStackTrace();
  return null;
} finally {
  // データベース切断
  if (conn != null) {
    try {
      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
}
public  List<ShopPostDataBeans> findAll(int id) {
  Connection conn = null;
  List<ShopPostDataBeans> shopPostList = new ArrayList<ShopPostDataBeans>();

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT * FROM post_data  inner join shop on post_data.shop_id = shop.shop_id where id = ?";

    // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setInt(1,id);
    ResultSet rs = pStmt.executeQuery();

    // 結果表に格納されたレコードの内容を
    // Userインスタンスに設定し、ArrayListインスタンスに追加
    while (rs.next()) {
      int shopId = rs.getInt("shop_id");
      String shopName= rs.getString("shop_name");
      int score= rs.getInt("score");
      int service = rs.getInt("service");
      int volume = rs.getInt("volume");
      int atomsphere = rs.getInt("atomsphere");
      int clean = rs.getInt("clean"); 
      int price = rs.getInt("price");
      int taste= rs.getInt("taste");
      String place = rs.getString("place");
      int Id=rs.getInt("id");
      String file = rs.getString("file");
      int postId=rs.getInt("post_id");
      String body = rs.getString("body");


      ShopPostDataBeans shop =
          new ShopPostDataBeans(shopId,shopName, score, service, volume, atomsphere,clean,price, taste,  place,
              Id,file,postId,body);

      shopPostList.add(shop);
    }
  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
  return shopPostList;
}


public void updatePost(String shopName,int score, int service,int volume ,int price, int clean,int taste,int atomsphere, String body,int postId) {
  Connection conn = null;

  try{ 
    conn = DBManager.getConnection();
    
    String sql = "UPDATE post_data  SET score=?,service=?,volume=?,price=?,clean=?,taste=?,atomsphere=?,body=? WHERE post_id=?";


    PreparedStatement stmt = conn.prepareStatement(sql);
    stmt.setString(1, shopName);
    stmt.setInt(2, score);
    stmt.setInt(5, service);
    stmt.setInt(6, volume);
    stmt.setInt(3,price);
    stmt.setInt(4, clean);
    stmt.setInt(7, taste);
    stmt.setInt(8,atomsphere);
    stmt.setString(9,body);
    stmt.setInt(10, postId);
    stmt.executeUpdate();


    String sql2 =
       " update shop_data set score_average=(select avg(score) from post_data), price_average=(select avg(price) from post_data), "
       + "service_average=(select avg(service) from  post_data),atomsphere_average=(select avg(atomsphere) from post_data),"
       + "clean_average=(select avg(clean) from post_data),volume_average)=select avg(volume) from post_data)";
    
    Statement Stmt = conn.createStatement();
    Stmt.executeUpdate(sql2);

  } catch (SQLException e) {
    e.printStackTrace();

  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

}

public void deletePost(int postId) {
  Connection conn = null;
  try {
    conn = DBManager.getConnection();

    String sql = "DELETE post_data,shop_data,shop FROM post_data left join shop_data on post_data.shop_id = shop_data.shop_id left join shop on post_data.shop_id = shop.shop_id  WHERE post_data.post_id=?;";

    PreparedStatement stmt = conn.prepareStatement(sql);
    stmt.setInt(1, postId);
    stmt.executeUpdate();
   
  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
  }
  }   
}
public  List<ShopPostDataBeans> findShopAll(int shopId) {
  Connection conn = null;
  List<ShopPostDataBeans> shopPostList = new ArrayList<ShopPostDataBeans>();

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT * FROM ( post_data as a ) inner join user as b  on a.id = b.id where shop_id = ?";

    // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setInt(1,shopId);
    ResultSet rs = pStmt.executeQuery();

    // 結果表に格納されたレコードの内容を
    // Userインスタンスに設定し、ArrayListインスタンスに追加
    while (rs.next()) {
      int _shopId = rs.getInt("shop_id");
      String shopName= rs.getString("shop_name");
      int score= rs.getInt("score");
      int service = rs.getInt("service");
      int volume = rs.getInt("volume");
      int price = rs.getInt("price");
      int taste= rs.getInt("taste");
      String place = rs.getString("place");
      int Id=rs.getInt("id");
      int clean = rs.getInt("clean");
      int atomsphere=rs.getInt("atomsphere");
      String file = rs.getString("file");
      int postId=rs.getInt("post_id");
      String body = rs.getString("body");
      String userName = rs.getString("user_name");


      ShopPostDataBeans shop =
          new ShopPostDataBeans(_shopId,shopName,score ,service,volume,atomsphere,clean
              ,price , taste, place,Id, file,postId,body,userName);

      shopPostList.add(shop);
    }
  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
  return shopPostList;
}
public ShopPostDataBeans findByPostName(String name) {
Connection conn = null;

try {
  // データベースへ接続
  conn = DBManager.getConnection();

  // SELECT文を準備
  String sql = "SELECT*FROM post_data WHERE shop_name = ?";

  // SELECTを実行し、結果表を取得
  PreparedStatement pStmt = conn.prepareStatement(sql);
  pStmt.setString(1,name);
  ResultSet rs = pStmt.executeQuery();

    if (!rs.next()) {
      return null;
    }

    int shopId = rs.getInt("shop_id");
    String ShopName= rs.getString("shop_name");
    int _postId = rs.getInt("post_id"); 
    int score = rs.getInt("score");
    int service = rs.getInt("service");
    int volume = rs.getInt("volume"); 
    int atomsphere = rs.getInt("atomsphere");
    int clean = rs.getInt("clean"); 
    int price = rs.getInt("price");
    int taste = rs.getInt("taste");
    int id = rs.getInt("id");
    String place = rs.getString("place");
    String file = rs.getString("file");
    String body=rs.getString("body");
   
  return new ShopPostDataBeans(shopId,ShopName, score, service, volume, atomsphere,clean,price, taste,  place,
      id,file,_postId,body);


} catch (SQLException e) {
  e.printStackTrace();
  return null;
} finally {
  // データベース切断
  if (conn != null) {
    try {
      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
}
public ShopPostDataBeans findByPostUserName(int postId) {
Connection conn = null;

try {
  // データベースへ接続
  conn = DBManager.getConnection();

  // SELECT文を準備
  String sql = "SELECT * FROM user as b inner join post_data as a on a.id = b.id WHERE post_id = ?";

  // SELECTを実行し、結果表を取得
  PreparedStatement pStmt = conn.prepareStatement(sql);
  pStmt.setInt(1,postId);
  ResultSet rs = pStmt.executeQuery();

    if (!rs.next()) {
      return null;
    }

    
    String userName= rs.getString("name");
    int _postId = rs.getInt("post_id"); 
    int id = rs.getInt("id");
    
  return new ShopPostDataBeans(userName,_postId,id);


} catch (SQLException e) {
  e.printStackTrace();
  return null;
} finally {
  // データベース切断
  if (conn != null) {
    try {
      conn.close();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
}

public ShopPostDataBeans findByShopld(int shopId) {
  Connection conn = null;

  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT*FROM post_data WHERE shop_id = ?";

    // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setInt(1,shopId);
    ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int _shopId = rs.getInt("shop_id");
      String ShopName= rs.getString("shop_name");
    
    return new ShopPostDataBeans(_shopId,ShopName);


  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
}
}