<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>女性のためのラーメンデータベース | 投稿削除</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" >ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    
    <form method="post" action="Logout">
    <div class="container">
        <div class="row">
            <div class="col-6 offset-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">ログアウトします</h3>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-danger">
                            ログアウトしてよろしいですか？
                        </p>
                        

                        
                                <!-- スレッドとコメントを削除するフォーム -->
                                
                                    <!-- 削除するThreadのIDをhiddenでもたせる -->
                                    <input type="hidden" name="postId" value="${postId}">
                                       <input type="hidden" name="user" value="${userInfo}">
                                    <button type="submit" class="btn btn-danger btn-block">ログアウトする</button>
                                </form>
                            </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="container">
                       <div class="row ">
                            <div class="col-6 offset-3 mt-4">
                                <!--スレッド表示・コメント投稿ページへ戻るリンク -->
                                <a href="Top?user=${userInfo.name}" class="btn btn-outline-primary btn-block">戻る</a>
                            </div>
                            </div>
                            </div>
</body>

</html>