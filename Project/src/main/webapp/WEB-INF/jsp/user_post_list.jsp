<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

   <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>女性のためのラーメンデータベース | ホーム</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- home.cssの読み込み -->
    <link href="css/home.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link" >${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-4">
                <a href="UserUpdate"><h2>${user.name}さん</h2></a>
            </div>
            <div class="col-4">
                <h2>${user.age}歳</h2>
            </div>
            <div class="col-4">
            　　<c:if test="${user.gender == 0}" >
                <h2>男</h2>
                </c:if>
                <c:if test="${user.gender == 1}" >
                <h2>女</h2>
                </c:if>
            </div>
            
        </div>

        <div class="row">
            <div class="col mt-3 d-flex justify-content-between">
                <h3 class="text-center">投稿一覧</h3>
                <!-- スレッド追加リンク-->
                <p><a class="text-primary" href="ShopAdd">投稿する</a></p>
                </li>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th>店名</th>
                                <th>点数</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- スレッド表示用 for文 -->
                            <c:forEach var="post" items="${shopPostList}">
                                <tr>
                                    <input type="hidden" name="postId" value="${post.postId}">
                                    <td class="col-9">
                                       <a href="ShopPostWatch?postId=${post.postId}"
                                            class="text-primary">${post.shopName}</a>  
                                    </td>
                                    <!-- スレッド表示・コメント投稿ページ リンク -->
                                    <td>${post.score}</td>
                                    <td class="text-right col-1">
                                        <!-- スレッド更新ボタン 表示制御 -->
                                        <c:if test="${userInfo.id == post.id}">
                                            <!-- スレッド更新ボタン -->
                                            <a type="button" class="btn btn-success"
                                                href="ShopPostUpdate?postId=${post.postId}">更新</a>
                                        <!-- スレッド削除ボタン 表示制御 -->
                                            <!-- スレッド削除ボタン -->
                                            <a type="button" class="btn btn-danger"
                                                href="ShopPostDelete?postId=${post.postId}">削除</a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</body>

</html>