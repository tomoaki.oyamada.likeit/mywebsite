<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>女性のためのラーメンデータベース | 店舗投稿一覧</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- home.cssの読み込み -->
    <link href="css/home.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-center">検索店舗投稿一覧</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th>店名</th>
                                <th>点数</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- スレッド表示用 for文 -->
                            <c:forEach var="shop" items="${shopId.shopList}">
                                <tr>
                                    <td class="col-9">
                                        <!-- スレッド表示・コメント投稿ページ リンク -->
                                        <a href="ShopPostWatch?postId=${shop.postId}"
                                            class="text-primary">${shop.name}</a>
                                    </td>
                                    <!-- スレッド表示・コメント投稿ページ リンク -->
                                    <td>${shop.score}</td>
                                    <td class="text-right col-1">
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</body>

</html>