<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

   <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>女性のためのラーメンデータベース | 投稿閲覧</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
            <!-- top.css 読み込み -->
            <link href="css/top.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>


    <div class="container mt-6">

        <div class="row">
            <div class="col-6 offset-3">
                <!-- エラーメッセージ start -->
                <c:if test="${errMsg != null}">
                    <div class="alert alert-danger" role="alert">${errMsg}</div>
                </c:if>
                <!--エラーメッセージ  end  -->
            </div>
        </div>

        
                <div class="col-5">
                    <h2><a href="UserPostList?user=${user.userName}"  class="text-primary">
                          ${user.userName}さんの投稿</a></h2>
                </div>
           

        <div class="row">
            <div class="col-6 offset-3 bgc2">
                <!--   スレッド投稿フォーム   -->
                <div class="form-group row">
                        <label for="title" class="control-label col-2">店名</label>
                        <div class="col-5">
                            <!-- 店名入力 -->
                                <h3
                                            class="text-primary">${postId.shopName}</h3>
                        </div>
                       
                        <div class="col-3">
                            <!-- 点数入力 -->
                            <h3>${postId.score}点</h3>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">金額</label>
                        <div class="col-10">
                            <!-- 金額 -->
                                <h5>${postId.price}円</h5>
                                </div>
                        </div>
                    
                    
                    <div class="form-group row">
                        <label for="shop-plase" class="control-label col-3">	都道府県</label>
                        <div class="col-9">
                            <!-- 都道府県 -->
                            ${postId.place}
                        </div>
                
                        <!--行きやすさ-->
                     
                                <label for="useful" class="control-label col-3"> 接客</label>
                                <div class="col-9">
                             ${postId.service}点
                          </div>
                        
                        
                        <!--量-->
                        
                            <label for="volume" class="control-label col-3"> 量</label>
                            <div class="col-9">
                          ${postId.volume}点
                        </div>
                        
                   
                            <label for="atomsphere" class="control-label col-3"> 女性の入りやすさ</label>
                            <div class="col-9">
                          ${postId.volume}点
                        </div>
                        
                   
                            <label for="clean" class="control-label col-3"> 内装</label>
                            <div class="col-9">
                          ${postId.clean}点
                        </div>
                        
                   
                  
                    <label for="taste" class="control-label col-3">味</label>
                    <div class="col-9">
                        <!--   味  -->
                      <c:if test="${postId.taste == 0}" var="flg" />

                       <c:if test="${flg}" >
                            あっさり
                          </c:if>
　　　　　　　　　　　  <c:if test="${!flg}" >
　　　　　　　　　　　　　　　　こってり
                           </c:if>
                    </div>
                    </div>

                    <div class="row">
                        <label for="picture" class="control-label col-3"></label>

                     ${postId.file}
                   
                    </div>

                    <div class="form-group row">
                        <label for="body" class="control-label col-2">本文</label>
                        <div class="col-10">
                            <!-- スレッドの本文 入力 -->
                            <textarea class="form-control" id="body" name="body" rows="4">${postId.body}</textarea>
                        </div>
                    </div>
                   
                <div class="row mt-3">
                    <div class="col">
                        <!-- スレッド一覧ページに戻るリンク -->
                        <a href="ShopSearch" class="text-primary">戻る</a>
                    </div>
                </div>
            </div>

</body>

</html>