<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>女性のためのラーメンデータベース | 投稿</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link" >${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>


    <div class="container mt-6">
        <div class="row">
            <div class="col-3 mx-auto">
                <h3>投稿</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-3">
                <!-- エラーメッセージ start -->
                <c:if test="${errMsg != null}">
                    <div class="alert alert-danger" role="alert">${errMsg}</div>
                </c:if>
                <c:if test="${postMsg != null}">
                <div class="alert alert-info" role="alert">${postMsg}</div>
                 </c:if>
                <!--エラーメッセージ  end  -->
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-3">
                <!--   スレッド投稿フォーム   -->
                <form method="post" action="ShopPostAdd" enctype="multipart/form-data">
               
                <div class="form-group row">
               
                        <label for="name" class="col-2 col-form-label">店名</label>
                        <div class="col-6">
                            <!-- 入力 -->
                            <input type="text" name="name" class="form-control" id="shop-name" value="${inputName}">
                        </div>
                    </div>
                     
                    <div class="form-group row">
               
                        <label for="score" class="col-2 col-form-label">点数</label>
                        <div class="col-6">
                            <!-- 点数入力 -->
                            <input type="number" name="score" class="form-control"  min="0" max="100" id="shop-score" value="${inputScore}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">金額</label>
                        <div class="col-10">
                            
                                <!-- 金額 -->
                                    <input type="text" name="price" class="form-control" id="price" value="${inputPrice}">
                                </div>
                        </div>
                    
                
                        <!--接客-->
                        <div class="row">
                                <label for="useful" class="control-label col-3"> 接客</label>
                                <div class="col-9">
                            <input type="range" name="service" class="form-control"value="${inputService}"  min="0" max="100">
                          </div>
                        </div>
                        
                        
                        <!--量-->
                        <div class="row">
                            <label for="volume" class="control-label col-3"> 量</label>
                            <div class="col-9">
                        <input type="range" name="volume" class="form-control" value="${inputVolume}" min="0" max="100">
                        </div>
                        </div>
                        
                          <div class="row">
                            <label for="atomsphere" class="control-label col-3"> 雰囲気</label>
                            <div class="col-9">
                        <input type="range" name="atomsphere" class="form-control" value="${inputAtomsphere}" min="0" max="100">
                        </div>
                        </div>
                        
                        
                          <div class="row">
                            <label for="clean" class="control-label col-3"> 清潔感</label>
                            <div class="col-9">
                        <input type="range" name="clean" class="form-control" value="${inputClean}" min="0" max="100">
                        </div>
                        </div>
                        
                        
                   
                  <div class="row">
                    <label for="taste" class="control-label col-3">味</label>
                    <div class="col-9">
                        <!--   味  -->
                  <input type="radio" name="taste" value="0">あっさり
                  <input type="radio" name="taste" value="1">こってり
                    </div>
                    </div>

                     <!-- 写真 -->
                    <div class="row">
                        <label for="picture" class="control-label col-4"> 写真</label>
                      <label for="file" class="control-label col-8">アップロードするファイルを選択してください</label>
                    </div>
                    <div class="row">
                          <div class="col-12">
                            <input type="file" id="file" value="${inputFile}" name="file" accept="image/*">
                          </div>
                          
                    </div>

                    <div class="form-group row mt-3">
                        <label for="body" class="control-label col-2">本文</label>
                        <div class="col-10">
                            <!-- スレッドの本文 入力 -->
                            <textarea class="form-control" id="body" name="body" rows="4">${inputBody}</textarea>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary btn-block form-submit">投稿</button>
                    </div>
                </form>
           
                
                     
           
                 <a class="btn btn-success container  my-2"  href="UserPostList?user=${userInfo.name}">投稿一覧</a>
                 </div>
                 </div>
              
               
            </div>

</body>

</html>