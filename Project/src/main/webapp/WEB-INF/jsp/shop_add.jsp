<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>女性のためのラーメンデータベース | 店舗情報検索画面</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="Logout">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>


    <div class="container mt-6">
        <div class="row">
            <div class="col-6 text-center mx-auto">
                <h3>店舗情報検索画面</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-3">
                <!-- エラーメッセージ start -->
                <c:if test="${errMsg != null}">
                    <div class="alert alert-danger" role="alert">${errMsg}</div>
                </c:if>
                <c:if test="${postMsg != null}">
                   <div class="alert alert-info" role="alert">${postMsg}</div>
                 </c:if>
                <!--エラーメッセージ  end  -->
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-3">
                <!--   スレッド投稿フォーム   -->
                <form method="post" action="ShopAdd">
                    
                    <div class="form-group row">
                        <label for="title" class="control-label col-2">店名</label>
                        <div class="col-8">
                            <!-- 店名入力 -->
                            <input type="text" name="name" class="form-control" id="name" value="${inputName}">
                        </div>
                       </div>
                    
                    <div class="form-group row mt-3">
                        <label for="shop-plase" class="control-label col-3">	都道府県</label>
                        <div class="col-9">
                            <!-- 都道府県 -->
                    
                <select name="place"value="${inputPlace}">
                    <option value="" selected></option>
                    <option value="北海道">北海道</option>
                    <option value="青森県">青森県</option>
                    <option value="岩手県">岩手県</option>
                    <option value="宮城県">宮城県</option>
                    <option value="秋田県">秋田県</option>
                    <option value="山形県">山形県</option>
                    <option value="福島県">福島県</option>
                    <option value="茨城県">茨城県</option>
                    <option value="栃木県">栃木県</option>
                    <option value="群馬県">群馬県</option>
                    <option value="埼玉県">埼玉県</option>
                    <option value="千葉県">千葉県</option>
                    <option value="東京都">東京都</option>
                    <option value="神奈川県">神奈川県</option>
                    <option value="新潟県">新潟県</option>
                    <option value="富山県">富山県</option>
                    <option value="石川県">石川県</option>
                    <option value="福井県">福井県</option>
                    <option value="山梨県">山梨県</option>
                    <option value="長野県">長野県</option>
                    <option value="岐阜県">岐阜県</option>
                    <option value="静岡県">静岡県</option>
                    <option value="愛知県">愛知県</option>
                    <option value="三重県">三重県</option>
                    <option value="滋賀県">滋賀県</option>
                    <option value="京都府">京都府</option>
                    <option value="大阪府">大阪府</option>
                    <option value="兵庫県">兵庫県</option>
                    <option value="奈良県">奈良県</option>
                    <option value="和歌山県">和歌山県</option>
                    <option value="鳥取県">鳥取県</option>
                    <option value="島根県">島根県</option>
                    <option value="岡山県">岡山県</option>
                    <option value="広島県">広島県</option>
                    <option value="山口県">山口県</option>
                    <option value="徳島県">徳島県</option>
                    <option value="香川県">香川県</option>
                    <option value="愛媛県">愛媛県</option>
                    <option value="高知県">高知県</option>
                    <option value="福岡県">福岡県</option>
                    <option value="佐賀県">佐賀県</option>
                    <option value="長崎県">長崎県</option>
                    <option value="熊本県">熊本県</option>
                    <option value="大分県">大分県</option>
                    <option value="宮崎県">宮崎県</option>
                    <option value="鹿児島県">鹿児島県</option>
                    <option value="沖縄県">沖縄県</option>
	                  </select>
	                        </div>
	                        </div>
	                   
	                   <div class="row">
                        <div class="col-12 ">
	             
	                        <button type="submit" class="btn btn-primary btn-block form-submit" >投稿</button>
	                    </div>
	                </form>
	                
	   
            </div>
</body>

</html>