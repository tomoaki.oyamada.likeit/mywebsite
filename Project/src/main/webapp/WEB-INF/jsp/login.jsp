<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>女性のためのラーメンデータベース | ログイン</title>
	<!-- header.cssの読み込み -->
	<link href="css/header.css" rel="stylesheet" type="text/css" />
	<!-- Bootstrapの読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active">
					<!-- ページタイトル、スレッド一覧へのリンク -->
					<a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
				</li>
			</ul>
		
		</nav>
	</header>

	<div class="container">
		<div class="row">
			<div class="col">
				<h3 class="text-center">ログイン</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!--   ログインフォーム   -->
				<form method="post" action="Login">

					<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<!--ログインID入力 -->
							<input type="text" name="loginId" id="loginId" class="form-control" value="${loginId}">
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<!--パスワード入力-->
							<input type="password" name="password" id="password" class="form-control" value="${user.password}">
						</div>
					</div>

					<div>
						<button type="submit" class="btn btn-primary btn-block form-submit">ログイン</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>

</html>