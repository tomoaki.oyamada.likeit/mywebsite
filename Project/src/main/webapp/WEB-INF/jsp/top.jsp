<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>	
        <meta charset="UTF-8">
        <title>女性のためのラーメンデータベース | トップ</title>
        <!-- header.cssの読み込み -->
        <link href="css/header.css" rel="stylesheet" type="text/css" />
        <!-- top.css 読み込み -->
        <link href="css/top.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrapの読み込み -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <!-- ヘッダー -->
        <header>
            <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
                <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                    <li class="nav-item active">
                        <a class="nav-link" href="Top">女性のためのラーメンデータベース</a>
                    </li>
                </ul>
                <ul class="navbar-nav flex-row">
                    <li class="nav-item"><a class="nav-link" href="UserAdd">ユーザ登録</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="Login">ログイン</a>
                    </li>
                </ul>
            </nav>
           
        <div class="container  my-3">
                <div class="col-12">
                    <div class="row">
                         <div class="col-6">
                        <a href="ShopSearch"><h1 class="p-md-3">🔍検索</h1></a>
                        </div>
                        <div class="col-6  p-md-3 ">
                            <a type="button" class="btn  btn-outline-danger"
                                                href="ShopAdd"><h2>投稿</h2></a>
                        </div>
                    </div>
                </div>
        </div>
            

            <div class="container  offset-0 bgc2 rounded">

            <h2 class="bgc3 rounded p-md-3 text-left">最新人気ラーメン店ランキング</h2>
            <div class="row ">
                <div class="col-4">
                 <h2><span class="bg-warning col-1 text-center ts">1位</span></h2>
                  <div class="offset-1"><a href="ShopSearchResult?shopId=${shop1.shopId}">
                <u><h2 class="ts">${shop1.shopName}</h2></u></a>
                 </div>
            <div class="card" style="width: 18rem;">
                <img src="Image?shopId=${shopId}" alt="ラーメン">
                <div class="card-body">
                  <h5 class="card-title">${rank1.scoreAverage}点</h5>
                  <p class="card-text">🏳️${shop1.place }</p>
                  <a href="ShopSearchResult?shopId=${rank1.shopId}" class="btn btn-primary">詳細ページへ</a>
                </div>
            </div>
            </div>

            
                <div class="col-4">
            <h2><span class="secondC col-1 text-center ts">2位</span></h2>
                <div class="offset-1"><a href="ShopSearchResult?shopId=${shop2.shopId}">
                <u><h2 class="ts">${shop2.shopName}</h2></u>
                </a>
                </div>
            <div class="card" style="width: 18rem;">
                <img src="Image?shopId=${shopId2}" alt="ラーメン"/>
                <div class="card-body">
                  <h5 class="card-title">${rank2.scoreAverage}点</h5>
                  <p class="card-text">🏳️${shop2.place}</p>
                  <a href="ShopSearchResult?shopId=${rank2.shopId}" class="btn btn-primary">詳細ページへ</a>
                </div>
              </div>
            </div>
            
            <div class="col-4">
            <h2><span class="thirdC col-1 text-center ts">3位</span></h2>
                <div class="offset-1"><a href="ShopSearchResult?shopId=${shop3.shopId}">
                <u><h2 class="ts">${shop3.shopName}</h2></u>
                </a>
                </div>
            <div class="card" style="width: 18rem;">
              <img src="Image?shopId=${shopId3}" alt="ラーメン">
                <div class="card-body">
                  <h5 class="card-title">${rank3.scoreAverage}点</h5>
                  <p class="card-text">🏳️${shop3.place}</p>
                  <a href="ShopSearchResult?shopId=${rank3.shopId}" class="btn btn-primary">詳細ページへ</a>
                </div>
            </div>
              </div>
            </div>

            </div>
        </div>
                            
        </header>