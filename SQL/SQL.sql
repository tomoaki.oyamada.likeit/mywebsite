CREATE DATABASE ramen DEFAULT CHARACTER SET utf8;
USE ramen;

CREATE TABLE user(
id int(12) PRIMARY KEY UNIQUE KEY NOT NULL AUTO_INCREMENT,
login_id varchar(200) UNIQUE KEY DEFAULT NULL,
name varchar(50) DEFAULT NULL,
password varchar(200) NOT NULL,
age int(4) NOT NULL,
gender int(2) NOT NULL,
is_admin tinyint(1) NOT NULL DEFAULT '0');

CREATE TABLE shop(
shop_id int(12) PRIMARY KEY UNIQUE KEY NOT NULL AUTO_INCREMENT,
shop_name varchar(300) NOT NULL);

CREATE TABLE shop_data(
shop_id int(12) PRIMARY KEY UNIQUE KEY NOT NULL AUTO_INCREMENT,
shop_name varchar(300) NOT NULL,
score_average int(12) NOT NULL,
service_average int(12) NOT NULL,
volume_average int(12) NOT NULL,
price_average int(12) NOT NULL,
taste_average int(12) NOT NULL,
place varchar(300) NOT NULL); 

CREATE TABLE post_data(
id int(12) UNIQUE KEY NOT NULL,
shop_id int(12) UNIQUE KEY NOT NULL,
post_id int(12) PRIMARY KEY UNIQUE KEY NOT NULL AUTO_INCREMENT,
place varchar(300) NOT NULL,
score int(12) NOT NULL,
service int(12) NOT NULL,
volume int(12) NOT NULL,
price int(12) NOT NULL,
body varchar(1000) NOT NULL,
file_name varchar(256) NOT NULL);
